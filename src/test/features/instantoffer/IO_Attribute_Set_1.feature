# Instant Offer Field validations covered for fields - Title, PostalCode, 
# Province, State, Phone, MOCode, FirstName, LastName, EmailConsent, Email, City, 
# BounceXFields, BabyField, Address1, Address2, MobilePhone, Zipcode
Feature: Validating Field Validation for Sign Up services of "Instant Offer" template type. 


@smokeTest
Scenario Outline: IO001 - Instant Offer Template Test- Validate the valid response when valid input parameters is provided.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									           |
	|2        | 200  |SheetPath_InstantOffer   			|SheetName_InstantOffer_Attributes     |



@smokeTest
Scenario Outline: IO002 - Instant Offer Template Test- Validate the response when Blank Mobile Phone is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests with "blank" "mobilePhone" field 
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName					  										|Message			   			|
	|3        | 400  |SheetPath_InstantOffer   	|SheetName_InstantOffer_Attributes     |MOBILEPHONEREQUIRED  |
#	


@smokeTest
Scenario Outline: IO003 - Instant Offer Template Test- Validate the response when Invalid Mobile Phone is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  		   |
	|4        | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | MOBILEPHONEWITHLESSTHANMINLENGTH   |
	


@smokeTest
Scenario Outline: IO004 - Instant Offer Template Test- Validate the response when Mobile Phone Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  	    |
	|5        | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |MOBILEPHONEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: IO005 - Instant Offer Template Test- Validate the response when Mobile Phone less than Min Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  		   |
	|6        | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |MOBILEPHONEWITHLESSTHANMINLENGTH    |
	
	
@smokeTest
Scenario Outline: IO006 - Instant Offer Template Test- Validate the response when Mobile Phone having Two Hypens Invalid Format is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  			    |
	|7        | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |MOBILEPHONEWITHTWOHYPENSINVALIDFORMAT    |
		

@smokeTest
Scenario Outline: IO007 - Instant Offer Template Test- Validate the response when Mobile Phone having AlphaNumeric value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  	  |
	|8        | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |MOBILEPHONEWITHALPHANUMERIC    |


@smokeTest
Scenario Outline: IO008 - Instant Offer Template Test- Validate the response when Mobile Phone having 12Digit value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath								|SheetName					  								 |Message					 					|
	|9        | 400  |SheetPath_InstantOffer  |SheetName_InstantOffer_Attributes     |MOBILEPHONEWITH12DIGIT    |
	
	

@smokeTest
Scenario Outline: IO009 - Instant Offer Template Test- Validate the response when address1 with Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					     					|
	|10       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |ADDRESS1EXCEEDINGMAXLENGTH    |
	


@smokeTest
Scenario Outline: IO010 - Instant Offer Template Test- Validate the response when address1 with Alphanumeric is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message  |
	|11       | 200  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | success |
	


@smokeTest
Scenario Outline: IO011 - Instant Offer Template Test- Validate the response when address1 with SpecialCharacters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName									  				 |Message    |
	|12       | 200  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | success   |	
	


@smokeTest
Scenario Outline: IO012 - Instant Offer Template Test- Validate the response when address2 with Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName									  				 |Message					    				 |
	|13       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |ADDRESS2EXCEEDINGMAXLENGTH   |
	


@smokeTest
Scenario Outline: IO013 - Instant Offer Template Test- Validate the response when address2 with Alphanumeric is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  									|Message  |
	|14       | 200  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes      | success |
	


@smokeTest
Scenario Outline: IO013 - Instant Offer Template Test- Validate the response when address2 with SpecialCharacters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message    |
	|15       | 200  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | success   |	


	
	
@smokeTest
Scenario Outline: IO014 - Instant Offer Template Test- Validate the response when babyField Exceeding MaxValue is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message                       |
	|16       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | BABYFIELDEXCEEDINGMAXVALUE   |	
	
	
		
@smokeTest
Scenario Outline: IO015 - Instant Offer Template Test- Validate the response when babyField With LessThan MinValue is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message                          |
	|17       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | BABYFIELDWITHLESSTHANMINVALUE   |
	
# This will be valid if Baby Field is non mandatory		
#@smokeTest
#@currentTest1
#Scenario Outline: IO016 - Instant Offer Template Test- Validate the response when babyField Accepting Zero is sent in request.
#	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
#	When  When user hit "post" requests
#	Then  User should received response status <code>
#	And   User validated the response <Message> received
#	
#	Examples:	
#	|Rowindex | code |SheetPath							|SheetName					  |Message    |
#	|18       | 200  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | success   |	
#	
#@currentTest		
#@smokeTest
#Scenario Outline: IO017 - Instant Offer Template Test- Validate the response when babyField Count Less Than ChildrenCount is sent in request.
#	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
#	When  user hit "post" requests with BabyField less than children count
#	Then  User should received response status <code>
#	And   User validated the response <Message> received
#	
#	Examples:	
#	|Rowindex | code |SheetPath							|SheetName					  |Message                                |
#	|19       | 200  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | BABYFIELDCOUNTLESSTHANCHILDRENCOUNT   |		
	
		
@smokeTest
Scenario Outline: IO018 - Instant Offer Template Test- Validate the response when CmpDeploymentStrategy field exceeds MaxLength is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message                           |
	|20       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | CMPDEPLOYMENTSTRATEGYMAXLENGTH   |		
	
	
		
@smokeTest
Scenario Outline: IO019 - Instant Offer Template Test- Validate the response when CmpNameMaxLength field exceeds MaxLength is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message             |
	|21       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | CMPNAMEMAXLENGTH   |	
	
		
@smokeTest
Scenario Outline: IO020 - Instant Offer Template Test- Validate the response when CmpVariationID field exceeds MaxLength is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message                    |
	|22       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | CMPVARIATIONIDMAXLENGTH   |	
	
	
	
		
@smokeTest
Scenario Outline: IO021 - Instant Offer Template Test- Validate the response when CmpConcept field exceeds MaxLength is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message                |
	|23       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | CMPCONCEPTMAXLENGTH   |	
	
	
	
@smokeTest
Scenario Outline: IO022 - Instant Offer Template Test- Validate the response when CmpDevice field exceeds MaxLength is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message               |
	|24       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | CMPDEVICEMAXLENGTH   |	
	
	
	
@smokeTest
Scenario Outline: IO023 - Instant Offer Template Test- Validate the response when CmpFormat field exceeds MaxLength is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					 									 |Message               |
	|25       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | CMPFORMATMAXLENGTH   |		
	
	
	
@smokeTest
Scenario Outline: IO024 - Instant Offer Template Test- Validate the response when City field exceeds MaxLength is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath									|SheetName					  								 |Message                   |
	|26       |400   |SheetPath_InstantOffer   	|SheetName_InstantOffer_Attributes     | CITYEXCEEDINGMAXLENGTH   |		
	
	
		
@smokeTest
Scenario Outline: IO025 - Instant Offer Template Test- Validate the response when City Accepting Alphanumeric is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							 			|SheetName					  								 |Message    |
	|27       | 200  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | success   |		
	
	
@smokeTest
Scenario Outline: IO027 - Instant Offer Template Test- Validate the response when City Accepting SpecialCharacters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					 								   |Message    |
	|28       | 200  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | success   |		
	
		
@smokeTest
Scenario Outline: IO028 - Instant Offer Template Test- Validate the response when email Without At The Rate is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message                  |
	|29       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | EMAILWITHOUTATTHERATE   |	
	
		
@smokeTest
Scenario Outline: IO029 - Instant Offer Template Test- Validate the response when email Without At Dot is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message            |
	|30       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | EMAILWITHOUTDOT   |	
	
	
	
		
@smokeTest
Scenario Outline: IO030 - Instant Offer Template Test- Validate the response when email With No Prefix before At The Rate is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message                             |
	|31       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | EMAILWITHNOPREFIXBEFOREATTHERATE   |
	
	
		
@smokeTest
Scenario Outline: IO031 - Instant Offer Template Test- Validate the response when email With exceeding Max length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName													   |Message                               |
	|32       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | EMAILWITHEMAILIDEXCEEDINGMAXLENGTH   |	
	
		
@smokeTest
Scenario Outline: IO032 - Instant Offer Template Test- Validate the response when email With Multiple At the rate is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message                               |
	|33       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | EMAILWITHMULTIPLEATTHERATEINEMAIL    |	
	
		
@smokeTest
Scenario Outline: IO033 - Instant Offer Template Test- Validate the response when email With SpecialCharacters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					 								   |Message                        |
	|34       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | EMAILWITHSPECIALCHARACTERS    |		
	
		
@smokeTest
Scenario Outline: IO034 - Instant Offer Template Test- Validate the response when email With Spaces is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message            |
	|35       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | EMAILWITHSPACE    |	
	

		
@smokeTest
Scenario Outline: IO035 - Instant Offer Template Test- Validate the response when email With LIOding Spaces is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message     |
	|36       | 200  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | success    |	
	
		
@smokeTest
Scenario Outline: IO036 - Instant Offer Template Test- Validate the response when email With Trailing Spaces is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message     |
	|37       | 200  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | success    |	
	
		
@smokeTest
Scenario Outline: IO037 - Instant Offer Template Test- Validate the response when EMAILCONSENT2MANDATORY is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message           				 |
	|38       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | EMAILCONSENT2MANDATORY    |							
	


@smokeTest
Scenario Outline: IO038 - Instant Offer Template Test- Validate the response when FirstName with Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName													   |Message					    				   |
	|39       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |FIRSTNAMEEXCEEDINGMAXLENGTH    |
	


@smokeTest
Scenario Outline: IO039 - Instant Offer Template Test- Validate the response when FirstName with Alphanumeric is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message  |
	|40       | 200  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | success |
	


@smokeTest
Scenario Outline: IO040 - Instant Offer Template Test- Validate the response when FirstName with SpecialCharacters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message    |
	|41       | 200  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | success   |	
	
	
						
@smokeTest
Scenario Outline: IO041 - Instant Offer Template Test- Validate the response when LastName with Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					      				 |
	|42       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |LASTNAMEEXCEEDINGMAXLENGTH     |
	


@smokeTest
Scenario Outline: IO042 - Instant Offer Template Test- Validate the response when LastName with Alphanumeric is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message  |
	|43       | 200  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | success |
	


@smokeTest
Scenario Outline: IO043 - Instant Offer Template Test- Validate the response when LastName with SpecialCharacters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message    |
	|44       | 200  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     | success   |	
	
	

@smokeTest
Scenario Outline: IO044 - Instant Offer Template Test- Validate the response when MoCode Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					   					|
	|45       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |MOCODEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: IO045 - Instant Offer Template Test- Validate the response when MOCode less than Min Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					     					 |
	|46       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |MOCODELESSTHANMINIMUMLENGTH    |
	
	
@smokeTest
Scenario Outline: IO046 - Instant Offer Template Test- Validate the response when MO Code Alphanumeric is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message		 |
	|47       | 200  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |success    |
		

@smokeTest
Scenario Outline: IO047 - Instant Offer Template Test- Validate the response when MOCode having SpecialCharacters value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					  	  				 |
	|48       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |MOCODEWITHSPECIALCHARACTERS    |
	

@smokeTest
Scenario Outline: IO048 - Instant Offer Template Test- Validate the response when MOCode having HyphenValidFormat value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message	   |
	|49       | 200  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |success    |	
	

@smokeTest
Scenario Outline: IO049 - Instant Offer Template Test- Validate the response when MOCode having Hyphen InValidFormat value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message	                        |
	|50       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |MOCODEWITHHYPHENINVALIDFORMAT   |	
		
#Mo Code ends	


@smokeTest
Scenario Outline: IO050 - Instant Offer Template Test- Validate the response when Phone Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					   				 |
	|51       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |PHONEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: IO051 - Instant Offer Template Test- Validate the response when Phone less than Min Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					      				|
	|52       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |PHONEWITHLESSTHANMINLENGTH    |
	

@smokeTest
Scenario Outline: IO052 - Instant Offer Template Test- Validate the response when phone With Two Hypens InvalidFormat is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					  			    			 |
	|53       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |PHONEWITHTWOHYPENSINVALIDFORMAT    |
		

@smokeTest
Scenario Outline: IO053 - Instant Offer Template Test- Validate the response when Phone having AlphaNumeric value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					  	  	 |
	|54       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |PHONEWITHALPHANUMERIC    |
	

@smokeTest
Scenario Outline: IO054 - Instant Offer Template Test- Validate the response when Phone having 12Digit value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message	   					|
	|55       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |PHONEWITH12DIGIT    |	
	

@smokeTest
Scenario Outline: IO055 - Instant Offer Template Test- Validate the response when Phone having Single Hyphen InValidFormat value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message    |
	|56       | 200  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |success    |	
		
		

@smokeTest
Scenario Outline: IO056 - Instant Offer Template Test- Validate the response when PostalCode Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					  						  |
	|57       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |POSTALCODEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: IO057 - Instant Offer Template Test- Validate the response when PostalCode with Invalid Format is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					      				 |
	|58       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |POSTALCODEWITHINVALIDFORMAT    |
	


@smokeTest
Scenario Outline: IO058 - Instant Offer Template Test- Validate the response when PostalCode With SpecialCharacters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					  			    			|
	|59       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |POSTALCODEWITHSPECIALCHARACTER    |
		


@smokeTest
Scenario Outline: IO059 - Instant Offer Template Test- Validate the response when PostalCode with Invalid Format2 value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  							   |Message					  	 					  |
	|60       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |POSTALCODEWITHINVALIDFORMAT2    |
	

@smokeTest
Scenario Outline: IO060 - Instant Offer Template Test- Validate the response when PostalCode WITH INVALID FORMAT3 value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message	   										  |
	|61       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |POSTALCODEWITHINVALIDFORMAT3    |	
	
	

@smokeTest
Scenario Outline: IO061 - Instant Offer Template Test- Validate the response when province Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					   						|
	|62       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |PROVINCEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: IO062 - Instant Offer Template Test- Validate the response when province with AlphaNumeric is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					      					 |
	|63       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |PROVINCEACCEPTINGALPHANUMERIC    |
	


@smokeTest
Scenario Outline: IO063 - Instant Offer Template Test- Validate the response when province With SpecialCharacters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					  			   					  |
	|64       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |PROVINCEACCEPTINGSPECIALCHARACTERS    |
		

@smokeTest
Scenario Outline: IO064 - Instant Offer Template Test- Validate the response when province with Invalid value value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					  	  			|
	|65       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |PROVINCEWITHINVALIDVALUE    |
	
	

@smokeTest
Scenario Outline: IO065 - Instant Offer Template Test- Validate the response when State Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					  				 |
	|66       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |STATEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: IO066 - Instant Offer Template Test- Validate the response when state With LessThan Min Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					      				|
	|67       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |STATEWITHLESSTHANMINLENGTH    |
	


@smokeTest
Scenario Outline: IO067 - Instant Offer Template Test- Validate the response when state With Non Capital Valid Value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					  			    		 |
	|68       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |STATEWITHNONCAPITALVALIDVALUE    |
		


@smokeTest
Scenario Outline: IO068 - Instant Offer Template Test- Validate the response when state With Special Characters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  							   |Message					  	  				|
	|69       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |STATEWITHSPECIALCHARACTERS    |
	

@smokeTest
Scenario Outline: IO069 - Instant Offer Template Test- Validate the response when state With AlphaNumeric is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					 									 |Message	  							 |
	|70       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |STATEWITHALPHANUMERIC    |	
	

@smokeTest
Scenario Outline: IO070 - Instant Offer Template Test- Validate the response when PostalCode having Single Hyphen InValidFormat value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message    							 |
	|71       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |STATEWITHNUMERICVALUE    |	
		
		
	

@smokeTest
Scenario Outline: IO071 - Instant Offer Template Test- Validate the response when Title Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  							   |Message					   				 |
	|72       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |TITLEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: IO072 - Instant Offer Template Test- Validate the response when title Accepting Alphanumeric is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message    |
	|73       | 200  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |success    |
	


@smokeTest
Scenario Outline: IO073 - Instant Offer Template Test- Validate the response when title Accepting Special Characters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message	   |
	|74       | 200  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |success    |
		

	

@smokeTest
Scenario Outline: IO074 - Instant Offer Template Test- Validate the response when zip Exceeding Max Length is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					   			 |
	|75       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |ZIPEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: IO075 - Instant Offer Template Test- Validate the response when zip With Less Than MaxLength is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message					      			|
	|76       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |ZIPWITHLESSTHANMAXLENGTH    |
	

@smokeTest
Scenario Outline: IO076 - Instant Offer Template Test- Validate the response when zip With CharAndDigit InValid Value is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:		
	|Rowindex | code |SheetPath										|SheetName					 									 |Message					  			   				 |
	|77       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |ZIPWITHCHARANDDIGITINVALIDVALUE    |
		


@smokeTest
Scenario Outline: IO077 - Instant Offer Template Test- Validate the response when zip With Special Characters is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath								|SheetName					 								   |Message					  	  			|
	|78       | 400  |SheetPath_InstantOffer  |SheetName_InstantOffer_Attributes     |ZIPWITHSPECIALCHARACTERS    |
	


@smokeTest
Scenario Outline: IO078 - Instant Offer Template Test- Validate the response when zip With AlphaNumeric is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					 									 |Message	   						 |
	|79       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |ZIPWITHALPHANUMERIC    |	
	
	

@smokeTest
Scenario Outline: IO079 - Instant Offer Template Test- Validate the response when zip With Numeric Value  is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message   						 |
	|80       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |ZIPWITHNUMERICVALUE    |	
		
		
		

@smokeTest
Scenario Outline: IO080 - Instant Offer Template Test- Validate the response when zip With Numeric And Char Value  is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					 								   |Message    									  |
	|81       | 400  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |ZIPWITHNUMERICANDCHARVALUE    |	
			
	
@smokeTest
Scenario Outline: IO081 - Instant Offer Template Test- Validate the response when zip With 10Numeric Value  is sent in request.
	Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath										|SheetName					  								 |Message    |
	|82       | 200  |SheetPath_InstantOffer   		|SheetName_InstantOffer_Attributes     |success    |	
	

	
	
			
		
		