Feature: Validating Sign Up services for all template type and for all production templates


@smokeTest
Scenario Outline: ProductionTemplateTest- Validate Sign Up services for all template types
	Given User want to hit Sign up service for template <index> from <sheetPath> and <sheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <message> received
	And   User should validate all Field validations
	
	Examples:	
	|index | code |sheetPath												|sheetName					  				| message   |
	|10    | 200  |SheetPath_ProductionTemplate 		|SheetName_ProductionTemplate |SuccESs 		|
#	|5     | 200  |SheetPath_ProductionTemplate 		|SheetName_ProductionTemplate |success		|


	
