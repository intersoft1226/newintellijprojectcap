@mobile_acquisition_functional_suite
Feature: Validating Sign Up services for Mobile Acquisition template type.


@smokeTest
Scenario Outline: MA001 - Mobile Acquisition TemplateTest- Validate the valid response when valid input parameters is provided.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	And   User validated the <isNewUser> status
	And   User validated SMSResult status as <success>
	And 	User validated the <SMSResult_Error> message
	
	Examples:	 
	|Rowindex | code |SheetPath												|SheetName					 				  |Message   | isNewUser | success | SMSResult_Error |
	|2        | 200  |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition  | success  | true 		 | true 	 | [blank]       	 |



@smokeTest
Scenario Outline: MA002 - Mobile Acquisition TemplateTest- Validate the API response on passing new phone number.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	And   User validated the <isNewUser> status
	And   User validated SMSResult status as <success>
	And 	User validated the <SMSResult_Error> message
	
	Examples:	
	|Rowindex | code |SheetPath												|SheetName					 					|Message   | isNewUser | success | SMSResult_Error |
	|3        | 200  |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition  | success  | true 		 | true 	 | [blank]       	 |



#@smokeTest
#Scenario Outline: MA003 - Mobile Acquisition TemplateTest- Validate the API response on passing the previously used  phone number.
#	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
#	When  When user hit "post" requests
#	Then  User should received response status <code>
#	And   User validated the response <Message> received
#	
#	Examples:	
#	|Rowindex | code |SheetPath												|SheetName					  				|Message	|
#	|3        | 200  |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition  |success  |
	


@smokeTest
Scenario Outline: MA004 - Mobile Acquisition TemplateTest- Verify the api response on passing the opted in phone number( Existing phone number).
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath												|SheetName					  				|Message   | isNewUser | success | SMSResult_Error |
	|4        | 200  |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition  | success  | false 		 | true 	 | [blank]       	 |
	


@smokeTest
Scenario Outline: MA005 - Mobile Acquisition TemplateTest- Verify the api response on passing the opted out phone number( Existing phone number).
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath												|SheetName					 				  |Message	|
	|5        | 200  |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition  |success  |


	
@smokeTest
Scenario Outline: MA006 - Mobile Acquisition TemplateTest- Verify the api response on passing the black listed phone number.
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath												|SheetName					 				  |Message	|
	|6        | 200  |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition  |success  |
	
	

	

	
	
	
	
	