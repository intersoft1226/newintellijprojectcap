# Mobile Acquisition Field validations covered for fields - Children[1]																																	
Feature: Validating Field Validation of Sign Up services for Field "BabyField-Children" of template type Mobile Acquisition. 																																	
																																	
																															
@smokeTest																																	
Scenario Outline: MA086 - Mobile Acquisition Template Test- Validate the response when blank FIRSTNAMEUNKNOWN of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex  | code |SheetPath											   |SheetName							  				|Message					          			|		
	|86        | 400  |SheetPath_MobileAcquisition   		 |SheetName_MobileAcquisition     |CHILDRENFIRSTNAMEUNKNOWNBLANK    |																														
																																	
																																	
																																		
@smokeTest																																	
Scenario Outline: MA087 - Mobile Acquisition Template Test- Validate the response when blank FIRSTNAME of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName											  |Message								   |
	|87       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition      |CHILDRENFIRSTNAMEBLANK    |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: MA088 - Mobile Acquisition Template Test- Validate the response when blank HASDOB of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName								  		  |Message				 		    |			
	|88       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition      |CHILDRENHASDOBBLANK    |																														
																																	
																																
@smokeTest																																	
Scenario Outline: MA089 - Mobile Acquisition Template Test- Validate the response when blank DATE OF BIRTH of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName											  |Message				 				      |
	|89       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition   	  |CHILDRENDATEOFBIRTHBLANK 	  |																												
																																	
																																	
																																		
@smokeTest																																	
Scenario Outline: MA090 - Mobile Acquisition Template Test- Validate the response when DATEOF BIRTH of First children is  in FUTURE is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName					 						 |Message				          			   |	
	|90       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENWITHFUTUREDATEOFBIRTH    |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: MA091 - Mobile Acquisition Template Test- Validate the response when  EXPECTED BIRTH MONTH is BLANK of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName						  				  |Message				          		      |			
	|91       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition      |CHILDRENEXPECTEDBIRTHMONTHBLANK    |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: MA092 - Mobile Acquisition Template Test- Validate the response when ExpectedBirthYear = current year AND ExpectedMonth<currentMonth of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath											|SheetName							  			  |Message				       				         |			
	|92       | 400  |SheetPath_MobileAcquisition   |SheetName_MobileAcquisition      |CHILDRENWITHPASTEXPECTEDBIRTHMONTH    |																																
																																	
																																	
																																	
																																
@smokeTest																																	
Scenario Outline: MA093 - Mobile Acquisition Template Test- Validate the response when blank CHILDREN EXPECTED BIRTH YEAR of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath											|SheetName										   |Message				              		  |					
	|93       | 400  |SheetPath_MobileAcquisition 	|SheetName_MobileAcquisition     |CHILDRENEXPECTEDBIRTHYEARBLANK    |																															
																																	
																																		
@smokeTest																																	
Scenario Outline: MA094 - Mobile Acquisition Template Test- Validate the response when Future EXPECTED BIRTH YEAR of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												  |SheetName					  					 |Message				                    		|				
	|94       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENWITHFUTUREEXPECTEDBIRTHYEAR   |																														
																																	
# *****This is not implemented yet since cant able to set Null values																																	
#@smokeTest																																	
#Scenario Outline: MA095 - Mobile Acquisition Template Test- Validate the response when CHILDREN WITH GENDER NULL of First children is sent in request.																																	
#	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
#	When  When user hit "post" requests with "null" "gender" field																																
#	Then  User should received response status <code>																																
#	And   User validated the response <Message> received																																
#																																	
#	Examples:																																
#	|Rowindex | code |SheetPath							|SheetName					  |Message				       |																
#	|95       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENWITHGENDERNULL_1    |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: MA096 - Mobile Acquisition Template Test- Validate the response when Invalid Gender Value of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath											|SheetName					 					   |Message				   				          |			
	|96       | 400  |SheetPath_MobileAcquisition   |SheetName_MobileAcquisition     |CHILDRENWITHINVALIDGENDERVALUE    |																																
																																	
																																	
																																
@smokeTest																																	
Scenario Outline: MA097 - Mobile Acquisition Template Test- Validate the response when ExpectedBirthMonth < 0 of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath											|SheetName									  |Message				                 		   |						
	|97       | 400  |SheetPath_MobileAcquisition  	|SheetName_MobileAcquisition  |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH |																															
																																	
																																		
@smokeTest																																	
Scenario Outline: MA098 - Mobile Acquisition Template Test- Validate the response when ExpectedBirthMonth > 12 of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath								  				|SheetName					             |Message				                 				   |							
	|98       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH    |																														
																																	
																																
@smokeTest																																	
Scenario Outline: MA099 - Mobile Acquisition Template Test- Validate the response when ExpectedBirthYear < current year of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName									  |Message				            			     |			
	|99       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition  |CHILDRENWITHPASTEXPECTEDBIRTHMONTH    |																														
																																	
																																	
																																			
@smokeTest																																	
Scenario Outline: MA100 - Mobile Acquisition Template Test- Validate the response when blank FIRSTNAMEUNKNOWN of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												  |SheetName					             |Message					       					   |					
	|100      | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENFIRSTNAMEUNKNOWNBLANK_1    |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: MA101 - Mobile Acquisition Template Test- Validate the response when blank FIRSTNAME of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName										  |Message									   |	
	|101      | 400  |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition    |CHILDRENFIRSTNAMEBLANK_1    |																															
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: MA102 - Mobile Acquisition Template Test- Validate the response when blank HASDOB of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath						  						|SheetName					  					 |Message				  				 |																
	|102      | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENHASDOBBLANK_1    |																														
																																	
																																
@smokeTest																																	
Scenario Outline: MA103 - Mobile Acquisition Template Test- Validate the response when blank DATE OF BIRTH of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath						  						|SheetName					  					 |Message				    			      |																
	|103      | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENDATEOFBIRTHBLANK_1    |																														
																																	
																																	
																																		
@smokeTest																																	
Scenario Outline: MA104 - Mobile Acquisition Template Test- Validate the response when DATEOF BIRTH of Second children is in FUTURE is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName					 						 |Message				        				     |	
	|104      | 400  |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition     |CHILDRENWITHFUTUREDATEOFBIRTH_1    |																															
																																	
																																	
																																
@smokeTest																																	
Scenario Outline: MA105 - Mobile Acquisition Template Test- Validate the response when  EXPECTED BIRTH MONTH is BLANK of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath										|SheetName										  |Message				               				|				
	|105      | 400  |SheetPath_MobileAcquisition |SheetName_MobileAcquisition    |CHILDRENEXPECTEDBIRTHMONTHBLANK_1   |																																
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: MA106 - Mobile Acquisition Template Test- Validate the response when ExpectedBirthYear = current year AND ExpectedMonth<currentMonth of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName									     |Message				                        |																
	|106      | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENWITHPASTEXPECTEDBIRTHMONTH_1  |																														
																																	
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: MA107 - Mobile Acquisition Template Test- Validate the response when blank CHILDREN EXPECTED BIRTH YEAR of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName									  	 |Message				              				|																
	|107      | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENEXPECTEDBIRTHYEARBLANK_1    |																														
																																	

																															
@smokeTest																																	
Scenario Outline: MA108 - Mobile Acquisition Template Test- Validate the response when Future EXPECTED BIRTH YEAR of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName						  				 |Message				                    			 |																
	|108      | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENWITHFUTUREEXPECTEDBIRTHYEAR_1    |																														
																																	
# *****This is not implemented yet since cant able to set Null values																																	
#@smokeTest																																	
#Scenario Outline: MA109 - Mobile Acquisition Template Test- Validate the response when CHILDREN WITH GENDER NULL of Second children is sent in request.																																	
#	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
#	When  When user hit "post" requests with "null" "gender" field																																
#	Then  User should received response status <code>																																
#	And   User validated the response <Message> received																																
#																																	
#	Examples:																																
#	|Rowindex | code |SheetPath							|SheetName					  |Message				       |																
#	|109       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENWITHGENDERNULL_2    |																														
																																	
																																	
																																		
@smokeTest																																	
Scenario Outline: MA110 - Mobile Acquisition Template Test- Validate the response when Invalid Gender Value of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												  |SheetName					  					 |Message				               				|																
	|110      | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENWITHINVALIDGENDERVALUE_1    |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: MA111 - Mobile Acquisition Template Test- Validate the response when ExpectedBirthMonth < 0 of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName					  					 |Message				                      			 |																
	|111      | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH_1    |																														
																																	
																																
@smokeTest																																	
Scenario Outline: MA112 - Mobile Acquisition Template Test- Validate the response when ExpectedBirthMonth > 12 of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath							|SheetName					  |Message				                      |																
	|112       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH_1    |																														
																																	

																																		
@smokeTest																																	
Scenario Outline: MA113 - Mobile Acquisition Template Test- Validate the response when ExpectedBirthYear < current year of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath							|SheetName					  |Message				                   |																
	|113       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENWITHPASTEXPECTEDBIRTHMONTH_1    |																														
																																	
																																	
																															
# The below test scenarios were writter in last thats why S.No is not in serial.
																																																																																												
@smokeTest																																	
Scenario Outline: MA135 - Mobile Acquisition Template Test- Validate the response when blank FIRSTNAMEUNKNOWN of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												  |SheetName					             |Message					       					   |					
	|135      | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENFIRSTNAMEUNKNOWNBLANK_2    |																														
																																	
																																	
																																		
@smokeTest																																	
Scenario Outline: MA136 - Mobile Acquisition Template Test- Validate the response when blank FIRSTNAME of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName										  |Message									   |	
	|136      | 400  |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition    |CHILDRENFIRSTNAMEBLANK_2    |																															
																																	
																																	
																																
@smokeTest																																	
Scenario Outline: MA137 - Mobile Acquisition Template Test- Validate the response when blank HASDOB of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName					  					 |Message				    			 |																
	|137      | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENHASDOBBLANK_2    |																														
																																	
																																		
@smokeTest																																	
Scenario Outline: MA138 - Mobile Acquisition Template Test- Validate the response when blank DATE OF BIRTH of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName					  					 |Message				      				  |																
	|138      | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENDATEOFBIRTHBLANK_2    |																														
																																	
																																	
																																
@smokeTest																																	
Scenario Outline: MA139 - Mobile Acquisition Template Test- Validate the response when DATEOF BIRTH of Third children is  in FUTURE is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName					 						 |Message				        				     |	
	|139      | 400  |SheetPath_MobileAcquisition   	|SheetName_MobileAcquisition     |CHILDRENWITHFUTUREDATEOFBIRTH_2    |																															
																																	
																																	
																																
@smokeTest																																	
Scenario Outline: MA140 - Mobile Acquisition Template Test- Validate the response when  EXPECTED BIRTH MONTH is BLANK of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath										|SheetName										  |Message				               				|				
	|140      | 400  |SheetPath_MobileAcquisition |SheetName_MobileAcquisition    |CHILDRENEXPECTEDBIRTHMONTHBLANK_2    |																																
																																	
																																	
																																			
@smokeTest																																	
Scenario Outline: MA141 - Mobile Acquisition Template Test- Validate the response when ExpectedBirthYear = current year AND ExpectedMonth<currentMonth of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName					  					 |Message				                   				|																
	|141      | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENWITHPASTEXPECTEDBIRTHMONTH_2    |																														
																																	
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: MA142 - Mobile Acquisition Template Test- Validate the response when blank CHILDREN EXPECTED BIRTH YEAR of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName					  					 |Message				             				  |																
	|142      | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENEXPECTEDBIRTHYEARBLANK_2    |																														
																																	
																																
@smokeTest																																	
Scenario Outline: MA143 - Mobile Acquisition Template Test- Validate the response when Future EXPECTED BIRTH YEAR of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath							|SheetName					  |Message				                    |																
	|143       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENWITHFUTUREEXPECTEDBIRTHYEAR_2    |																														
																																	
# *****This is not implemented yet since cant able to set Null values																																	
#@smokeTest																																	
#Scenario Outline: MA144 - Mobile Acquisition Template Test- Validate the response when CHILDREN WITH GENDER NULL of Second children is sent in request.																																	
#	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
#	When  When user hit "post" requests with "null" "gender" field																																
#	Then  User should received response status <code>																																
#	And   User validated the response <Message> received																																
#																																	
#	Examples:																																
#	|Rowindex | code |SheetPath							|SheetName					  |Message				       |																
#	|144       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENWITHGENDERNULL_1    |																														
																																	
																																	
																															
@smokeTest																																	
Scenario Outline: MA145 - Mobile Acquisition Template Test- Validate the response when Invalid Gender Value of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath							|SheetName					  |Message				               |																
	|145       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENWITHINVALIDGENDERVALUE_2    |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: MA146 - Mobile Acquisition Template Test- Validate the response when ExpectedBirthMonth < 0 of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath							|SheetName					  |Message				                      |																
	|146       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH_2    |																														
																																	
																																	
@smokeTest																																	
Scenario Outline: MA147 - Mobile Acquisition Template Test- Validate the response when ExpectedBirthMonth > 12 of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath							|SheetName					  |Message				                      |																
	|147       | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH_2    |																														
																																	
																																
@smokeTest																																	
Scenario Outline: MA148 - Mobile Acquisition Template Test- Validate the response when ExpectedBirthYear < current year of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName					 						 |Message				                   				|																
	|148      | 400  |SheetPath_MobileAcquisition   		|SheetName_MobileAcquisition     |CHILDRENWITHPASTEXPECTEDBIRTHMONTH_2    |																														
																																	
																																
																															