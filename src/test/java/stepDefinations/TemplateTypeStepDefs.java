package stepDefinations;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.collections.bag.SynchronizedSortedBag;
import org.testng.annotations.BeforeSuite;
import org.testng.asserts.SoftAssert;

import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;
import com.google.protobuf.Message;
import com.google.protobuf.compiler.PluginProtos.CodeGeneratorResponse.File;

import common.CommonLib;
import common.DataBaseConnection;
import common.DataSet;
import common.FileLoggers;
import common.ReplaceFieldValuePayload;
import common.RequestAPI;
import common.ResponseAPI;
import common.Utils;
import common.ValidationMessages;
import cucumber.api.java.Before;
import fieldValidations.CustomCheckBox;
import fieldValidations.MobilePhone;
import gherkin.deps.com.google.gson.Gson;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import pojo.Custom;
import pojo.EmailAcquisition;
import cucumber.api.Scenario;

public class TemplateTypeStepDefs extends Utils {
	String payloads;
//    RequestSpecification res;
	String scenarioName;
	String methodName;
//	String resourceType;
	Response response;
	CommonLib reqspec1;
	String sheetName;
	String sheetPath;
	DataSet ds = new DataSet();
	String scenarioDescription;
	String updatedPayload;
	io.cucumber.core.api.Scenario scenario;

	@io.cucumber.java.Before
	public void before(io.cucumber.core.api.Scenario scenario) {
		this.scenario=scenario;
		scenarioName = scenario.getName();
	}
	
	public TemplateTypeStepDefs() {
		
	}


	public String getCustomFieldLabelName(String CustomFieldId) throws IOException, SQLException {
//		System.out.println(customFieldData);
		FileLoggers.info("*********** Get Custom Field Label Name   ****************************");
		for (Entry<String, String> entry : CommonLib.customFieldData.entrySet()) {
			if (entry.getKey().equals(CustomFieldId)) {
				System.out.println(entry.getValue());
				return entry.getValue();
			}
		}
		return null;
	}

	@Given("^User want to hit \"([^\"]*)\" template type SignUp service with template detail (.+) from (.+) and (.+)$")
	public void user_want_to_hit_something_template_type_signup_service_with_template_detail_from_and(
			String templateType, Integer rowIndex, String sheetPath, String sheetName) throws Throwable {		
		CommonLib.sheetPath = System.getProperty("user.dir") + getGlobalProperty(sheetPath);
		System.out.println(CommonLib.sheetPath);
		CommonLib.sheetName = getGlobalProperty(sheetName);
		CommonLib.rowIndex = rowIndex;
		//FileLoggers.initLogger();
		FileLoggers.startTestCase("scenarioName" + scenarioName);
		DataSet dset = new DataSet();
		CommonLib.payloads = dset.setData(CommonLib.sheetPath, CommonLib.sheetName, CommonLib.rowIndex);
		RequestAPI req = new RequestAPI();
		CommonLib.res = req.getRequestSpecification(CommonLib.payloads);
		scenario.write("***************  Request Payload below: *************");
		scenario.write(CommonLib.payloads);
	}

	@When("^user hit \"([^\"]*)\" requests with \"([^\"]*)\" \"([^\"]*)\" field$")
	public void user_hit_something_requests_with_something_customfieldid_field(String methodName, String fieldValue,
			String validationCustomFieldType) throws Throwable {
		List<String> data = ds.getDataList(CommonLib.sheetPath, CommonLib.sheetName, CommonLib.rowIndex);
		CommonLib.templateType = data.get(0);
		CommonLib.customFieldId = data.get(50);
		if (!fieldValue.equals("multiple")) {
			updatedPayload = convertCustomIdAsBlank(validationCustomFieldType);
			CommonLib.payloads = updatedPayload;
		}
		RequestAPI req = new RequestAPI();
		CommonLib.res = req.getRequestSpecification(CommonLib.payloads);
		ResponseAPI resp = new ResponseAPI();
		CommonLib.response = resp.hitRequest(CommonLib.templateType, methodName, CommonLib.res);
		FileLoggers.info(" **********  Reponse Message is below   ********");
		FileLoggers.info(CommonLib.response.asString());
		scenario.write("*************Response received is below: *****************");
		scenario.write("*********************  "+CommonLib.response.asString()+"     *****************");
	}

	@When("^user hit \"([^\"]*)\" requests with \"([^\"]*)\" \"([^\"]*)\" field	of \"([^\"]*)\" children$")
	public void user_hit_something_requests_with_something_something_fieldof_something_children(String methodName,
			String value, String fieldName, String childrenValue) throws Throwable {
		String updatedPayload;
		List<String> data = ds.getDataList(CommonLib.sheetPath, CommonLib.sheetName, CommonLib.rowIndex);
		CommonLib.templateType = data.get(0);
		fieldName = "genderThirdChild";
		String fieldValue = null;
		ReplaceFieldValuePayload pup = new ReplaceFieldValuePayload();
		updatedPayload = pup.replaceFieldValue(CommonLib.payloads, fieldName, fieldValue);
		FileLoggers.info("**********Updated Payload***");
		FileLoggers.info(updatedPayload);
		RequestAPI req = new RequestAPI();
		CommonLib.res = req.getRequestSpecification(updatedPayload);
		ResponseAPI resp = new ResponseAPI();
		CommonLib.response = resp.hitRequest(CommonLib.templateType, methodName, CommonLib.res);
		FileLoggers.info(" **********  Reponse Message is below   ********");
		FileLoggers.info(CommonLib.response.asString());
		scenario.write("*************Response received is below: *****************");
		scenario.write("*********************  "+CommonLib.response.asString()+"     *****************");
	}

}
