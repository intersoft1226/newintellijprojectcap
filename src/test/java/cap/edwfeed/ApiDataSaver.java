package cap.edwfeed;


import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.json.JSONObject;



import common.CommonLib;
import common.DataDriven;
import common.FileLoggers;
import common.Utils;

import gherkin.deps.com.google.gson.Gson;


public class ApiDataSaver extends Utils {
	String saverPath;
	String saverFile;
	String templateFile;
	DataDriven dwriter;
	String responseId;
	String templateName;
	String templateType;
	String tableName;
	public void requestResponseSaver(String payload, String Response) {
		
		FileLoggers.info(" **********  EDWFEED Process Started   ********");
		JSONObject responseJson = new JSONObject(Response);
		
	if	(responseJson.getInt("statusCode")==200) {
		FileLoggers.info(" ********** >>>>>>>>>>>>>Response is 200 hence, we are saving the payload, response, & template data ");
		
		try {
			saverPath=System.getProperty("user.dir") + getGlobalProperty("SheetPath_Saver");
			saverFile= getGlobalProperty("SheetName_Saver");
			templateFile= getGlobalProperty("SheetTemplate");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		dwriter = new DataDriven(saverPath);
		int rowcount= dwriter.getRowCount(saverFile);
		DateFormat date_format_obj = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        Date date_obj = new Date();
        System.out.println("Current date and time: " + date_format_obj.format(date_obj));
		
        
		responseId = responseJson.getString("responseId");
		
		JSONObject requestJson = new JSONObject(payload);
		templateName = requestJson.getString("id");
        
		dwriter.setCellData(saverFile, "ExecutionDate" , rowcount+1, date_format_obj.format(date_obj));
		dwriter.setCellData(saverFile, "responseId" , rowcount+1, responseId);
		dwriter.setCellData(saverFile, "Payload" , rowcount+1, payload);
		dwriter.setCellData(saverFile, "Response" , rowcount+1, Response);
		
		if (dwriter.getCellRowNum(templateFile,"TemplateName" , templateName)==-1) {
			
			
			FileLoggers.info(" ********** >>>>>>>>>>>>>Template doesn't exist in the file ");
			try {
				addTemplateData();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {
			
			FileLoggers.info(" ********** >>>>>>>>>>>>>Template exist in the file No need to hit db ");
		}
	}else {
		FileLoggers.info(" ********** >>>>>>>>>>>>>Status code is not 200 not saving response ");
	}
		
	}
	
	
		public void addTemplateData() throws SQLException, IOException {
		
			// Get the template type 
			String gettemplateTypeQuery = "SELECT templateType FROM template WHERE templateid = '"+templateName+"'"; 
			ResultSet queryresult =CommonLib.db.getSQLResults(gettemplateTypeQuery);
			queryresult.first();
			String templateType = queryresult.getString("templateType");	
			FileLoggers.info(" ********** >>>>>>>>>>>>>Template dType is : "+templateType);
			
			//get the table name for the template type 
			
			String templateTable = null;

			switch (templateType) {
			case "data-collection":
				templateTable = "data_collection";
				break;
			case "email":
				templateTable = "email";
				break;
			case "mobile":
				templateTable = "mobile";
				break;
			case "instant-offer":
				templateTable = "instant_offer";
				break;
			case "instant-offer-no-esp":
				templateTable = "instant_offer_no_esp";
				break;
			default:
				FileLoggers.info("Template type noy found");
			}
			
			
			
			//Create query
			String getTemplateDataQuery ="SELECT * FROM template T LEFT JOIN "+templateTable+" E ON T.templatekey = E.templatekey \r\n"
					+ "LEFT JOIN esp ES ON T.templatekey = ES.templatekey \r\n"
					+ "LEFT JOIN evs EV ON  T.templatekey = Ev.templatekey\r\n"
					+ "WHERE T.templateid = '"+templateName+"'";
			
			// get the template data 
			ResultSet templateData = null;
			templateData = CommonLib.db.getSQLResults(getTemplateDataQuery);
			ResultSetMetaData  metadata=templateData.getMetaData();
			int columns = metadata.getColumnCount();
			HashMap<String, Object> row = new HashMap<String, Object>();
			  while (templateData.next()) {
			     for (int i = 1; i <= columns; i++) {
			       row.put(metadata.getColumnName(i), templateData.getObject(i));
			     }
			  }
			  

			  Gson gson = new Gson(); 
			  String templatedataJson = gson.toJson(row);
//			  for (Map.Entry<String, Object> name: row.entrySet()) {
//				    String key = name.getKey();
//				    String value =  Objects.nonNull(name.getValue())?name.getValue().toString():null;
//				    System.out.println(key + ": " + value);
//				}

			int rowcountTemplatefile= dwriter.getRowCount(templateFile);
			dwriter.setCellData(templateFile, "TemplateName" , rowcountTemplatefile+1, templateName);
			dwriter.setCellData(templateFile, "TemplateType" , rowcountTemplatefile+1, templateType);
			dwriter.setCellData(templateFile, "ResultSet" , rowcountTemplatefile+1, templatedataJson);
			FileLoggers.info("!!!!!!!!!!!!!!!!!!!!Template data Added in excel sheet!!!!!!!!!!!!!!!!!!!!!!!!!!!");

			
		}
	

}
