package common;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;

import common.*;
import pojo.Children;
import pojo.Custom;
import pojo.EmailAcquisition;

public class DataSet {
	int colCount;
	int Flag = 1;
	String sheetPath;
	String sheetName;

	public List<String> getDataList(String sheetPath, String sheetName, int index) {
		List<String> lst = new ArrayList<String>();
		this.sheetPath = sheetPath;
		this.sheetName = sheetName;
		DataDriven d = new DataDriven(sheetPath);
		d.getColumnCount(sheetName);
		colCount = d.getColumnCount(sheetName);
		CommonLib.colCount=colCount; 
		for (int i = 1; i < colCount; i++) {
			// Checking for blank values and replacing blank with null.
			if ((d.getCellData(sheetName, i, index).isEmpty())) {
//				System.out.println(d.getCellData(SheetName, i-1, index));
				lst.add(null);
			} else {
				lst.add(d.getCellData(sheetName, i, index));
			}
		}
		return lst;
	}

	public String setData(String sheetPath, String sheetName, int rowNum)
			throws JsonProcessingException, ParseException {
		List<String> data = getDataList(sheetPath, sheetName, rowNum);
		System.out.println(data);
		EmailAcquisition ac = new EmailAcquisition();
		ac.setPostalCode(data.get(15));
		ac.setId(data.get(2));
		
		ac.setEmail(data.get(1));
		System.out.println(data.get(3));
		// Setting ZipCode
		String zipData = data.get(3);
		if (!sheetName.contains("UnPrintableCharacters")) {
			ac.setZipcode(zipData);
		} else {
			Double updZip = Double.parseDouble(zipData);
			int value123 = (updZip.intValue());
			String str4123 = Integer.toString(value123);
			ac.setZipcode(str4123);
		}

//		try {
//			if (zipData != null) {
//				try {
//					if(Integer.parseInt(zipData)>2147483647)
////				int zip = Integer.parseInt(zipData);
//				ac.setZipcode(zipData);
//				}catch (Exception e) {				
//				Double updZip = Double.parseDouble(zipData);
//				int value123 = (updZip.intValue());
//				String str4123 = Integer.toString(value123);
//				ac.setZipcode(str4123);
//			}
//		} 
//		}catch (NumberFormatException e) {
//			ac.setZipcode(data.get(3));
//		}
////		}	
		ac.setTitle(data.get(4));
		ac.setAddr1(data.get(5));
		ac.setAddr2(data.get(6));
		ac.setCity(data.get(7));
		ac.setDateQuestion(data.get(8));
		ac.setDateQuestionValue(data.get(9));
		ac.setState(data.get(10));
		ac.setFirstName(data.get(11));
		ac.setLastName(data.get(12));
//		ac.setGuardian(data.get(12));
		ac.setProvince(data.get(14));
		// Setting BounceXCustom Fields
		ac.setCmpDeploymentStrategy(data.get(43));
		ac.setCmpName(data.get(44));
		ac.setCmpVariationID(data.get(45));
		ac.setCmpConcept(data.get(46));
		ac.setCmpFormat(data.get(47));
		ac.setCmpDevice(data.get(48));
		ac.setSourceID(data.get(49));
		// Setting Guardian
		try {
			Double dblValue12 = Double.parseDouble(data.get(13));
			String str42 = String.format("%.2f", dblValue12);
			Double dbg = Double.parseDouble(str42);
			int value4 = (dbg.intValue());
			String str41 = Integer.toString(value4);
			ac.setGuardian(str41);
		} catch (Exception e) {
		}
		// Setting MobilePhone
		try {
			Double dblValue1 = Double.parseDouble(data.get(41));
			String str44 = String.format("%.0f", dblValue1);
			ac.setMobilePhone(str44);
//			Double db = Double.parseDouble(str44);	
//			int value44 = (db.intValue());
//			String str43 = Integer.toString(value44);
//			ac.setMobilePhone(str43);
		} catch (Exception e) {

			ac.setMobilePhone(data.get(41));
		}
		// Setting Phone
		try {
			Double dblValue1 = Double.parseDouble(data.get(16));
			String str3 = String.format("%.0f", dblValue1);
//			Double db = Double.parseDouble(str3);
//			int value = (db.intValue());
//			String str4 = Integer.toString(value);
			ac.setPhone(str3);
		} catch (Exception e) {
			ac.setPhone(data.get(16));
		}
		// Setting MOCode
		if(Objects.nonNull(data.get(17))){
			if(data.get(17).equalsIgnoreCase("db")) {
			MyOffer moobj = new MyOffer();
			ac.setMocode(moobj.getMOCode());
			}else {
				try {
					Double dblValue = Double.parseDouble(data.get(17));
					String str = String.format("%.2f", dblValue);
					Double db = Double.parseDouble(str);
					int value = (db.intValue());
					String str2 = Integer.toString(value);
					ac.setMocode(str2);
				} catch (Exception e) {
					ac.setMocode(data.get(17));
				}
			}
				
		}else{
			ac.setMocode(data.get(17));
		}
		// Setting Email Consent
		ac.setEmailconsent(data.get(40));
		// Setting Email Consent2
		ac.setEmailconsent2(data.get(42));
		// Setting BabyField
		if (data.get(18) != null && data.get(18) != "0") {
			String babydata = data.get(18);
			Double baby = Double.parseDouble(babydata);
			ac.setBabyFields(baby.intValue());
			// Setting Baby/Children Fields
			List<Children> myChildList = new ArrayList<Children>();
			String firstNameUnknown;
			String firstName;
			String hasDateOfBirth;
			String expectedBirthYear;
			String expectedBirthMonth;
			String dateOfBirth;
			String gender;
		
			if (baby.intValue() > 0) {
				for (int child = 19; child <= 36; child++) {
					if (Flag <= baby.intValue()) {
						firstNameUnknown = data.get(child);
						firstName = data.get(child + 1);
						hasDateOfBirth = data.get(child + 2);
						// ExpectedBirthYear
						String x = data.get(child + 3);
						try {
							Double by = Double.parseDouble(x);
							int yr = by.intValue();
							expectedBirthYear = String.valueOf(yr);
						} catch (Exception e) {
							expectedBirthYear = " ";
							FileLoggers
									.info("********** Error while assigning values to BabyFields of Children " + Flag);
						}
						// ExpectedBirthMonth
						try {
							String y = data.get(child + 4);
							Double month = Double.parseDouble(y);
							int mnth = month.intValue();
							expectedBirthMonth = String.valueOf(mnth);
						} catch (Exception e) {
							expectedBirthMonth = null;
							FileLoggers
									.info("********** Error while assigning values to BabyFields of Children " + Flag);
						}
						// Date of Birth converting "MM/dd/yy" format to "yyyy-MM-dd" format
						try {
							String date1 = data.get(child + 5);
							DateFormat formatter = new SimpleDateFormat("MM/dd/yy");
							Date date = (Date) formatter.parse(date1);
							SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
							dateOfBirth = formatter1.format(date);
						} catch (Exception e) {
							dateOfBirth = null;
							FileLoggers
									.info("********** Error while assigning values to BabyFields of Children " + Flag);
						}
						gender = data.get(child + 6);
						myChildList.add(new Children(firstNameUnknown, firstName, hasDateOfBirth, expectedBirthYear,
								expectedBirthMonth, dateOfBirth, gender));
						child = child + 6;
						Flag++;
					} else {
						break;
					}

				}
				ac.setChildren(myChildList);
			}
		}
//		else {
//			ac.setBabyFields(data.get(17));
//		}
//		 Setting Custom Fields
		List<Custom> mylist = new ArrayList<Custom>();
		String Id;
		String Value = null;
		for (int t = 50; t <= colCount - 2; t++) {
			Id = data.get(t); // test26
			Value = data.get(t + 1); // value26
			if (Id != null) {
				mylist.add(new Custom(Id, Value));
//				t++;
			} else {
				if(Value!=null) {
					mylist.add(new Custom(Id, Value));
				}
				else if(t==colCount) {
				break;
				}else {
					if(data.get(t)==null && data.get(t+1)==null) {
						break;
					}
				}
			}
			t++;
		}
		ac.setCustom(mylist);
		ObjectMapper obj = new ObjectMapper();
		obj.setSerializationInclusion(Include.NON_NULL);
		obj.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
		obj.registerModule(new Hibernate4Module());
		String datamap = obj.writerWithDefaultPrettyPrinter().writeValueAsString(ac);
		FileLoggers.info(datamap);
		return datamap;
	}
}
