package common;

import java.io.IOException;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class ResponseAPI extends Utils{
	Response response;	
	public Response hitRequest(String resourceType, String MethodName, RequestSpecification res)  {		
		APIResources resourceAPI = APIResources.valueOf(resourceType);
		ResponseSpecification resspec = responseSpecification();
		try {
			FileLoggers.info("*********  API EndPoint is --> "+getGlobalProperty("baseURL")+resourceAPI.getResource()+" *******");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response = res.when().post(resourceAPI.getResource()).then().spec(resspec).extract().response();
		return response;
		
	}
}
