package common;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import static io.restassured.RestAssured.*;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;

public class RequestAPI extends Utils{	
	RequestSpecification res;
	public RequestSpecification getRequestSpecification(String payloads) throws IOException {		
		res = RestAssured.given().spec(requestSpecification()).auth().preemptive()
				.basic(getGlobalProperty("Auth_UserName"), getGlobalProperty("Auth_Password"))
				.body(payloads);
		return res;
	}

}
