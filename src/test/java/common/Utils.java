package common;

import static org.testng.Assert.assertEquals;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;
import org.junit.Assert;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import gherkin.deps.com.google.gson.Gson;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import pojo.Custom;
import pojo.EmailAcquisition;

public class Utils {
	public static RequestSpecification req;
	// response validation detail starts
	String updatedPayload;
	String requestId;
	String responseId;
	String templateId;
	Response response;
	String resourceType;
//	int colCount;
	String Id;
	String Value = null;
//	String responsePage;
	JsonPath js;
	


	public RequestSpecification requestSpecification() throws IOException {
		if (req == null) {
			RestAssured.useRelaxedHTTPSValidation();
			RequestSpecification req = new RequestSpecBuilder().setBaseUri(getGlobalProperty("baseURL"))
					.setContentType(ContentType.JSON).build();
			return req;
		}
		return req;
	}

	public ResponseSpecification responseSpecification() {
		ResponseSpecification resSpec = new ResponseSpecBuilder().expectContentType(ContentType.JSON).build();
		return resSpec;
	}

	public static String getGlobalProperty(String key) throws IOException {
		java.util.Properties prop = new java.util.Properties();
		FileInputStream fis = new FileInputStream(
				//System.getProperty("user.dir") + "\\src\\test\\java\\resources\\global.properties");
		System.getProperty("user.dir") + "\\target\\test-classes\\global.properties");
		prop.load(fis);
		return prop.getProperty(key);
	}

	// Getting Total number of Rows from Result Set
	public int getRows(ResultSet res) {
		int totalRows = 0;
		try {
			res.last();
			totalRows = res.getRow();
			res.beforeFirst();
		} catch (Exception ex) {
			return 0;
		}
		return totalRows;
	}

//	 Response Validation
	public void responseValidation(String payload, String fieldName, String fieldValue, String resourceType,
			String methodName, int expectedStatusCode, String expectedErrorMessage) throws IOException {
		RequestAPI api = new RequestAPI();		
		ResponseAPI respapi = new ResponseAPI();
		JSONObject jsobj = new JSONObject(payload);
		templateId = jsobj.getString("id");
		ReplaceFieldValuePayload pup = new ReplaceFieldValuePayload();
		updatedPayload = pup.replaceFieldValue(payload, fieldName, fieldValue);
		req = api.getRequestSpecification(updatedPayload);
		response = respapi.hitRequest(resourceType, methodName, req);
		String actualResponse = response.asString();
		responseDataValidation( payload, resourceType, actualResponse, expectedErrorMessage);
	}

	// **********Custom Field Response Validation
	public void customResponseValidation(String updatedPayload, String fieldName, String fieldValue, String resourceType,
			String methodName, int expectedStatusCode, String expectedErrorMessage) throws IOException {
		RequestAPI api = new RequestAPI();
		ResponseAPI respapi = new ResponseAPI();
		JSONObject jsobj = new JSONObject(updatedPayload);
		templateId = jsobj.getString("id");
		req = api.getRequestSpecification(updatedPayload);
		response = respapi.hitRequest(resourceType, methodName, req);
		String actualResponse = response.asString();
		// Validating Status Code
		FileLoggers.info("Status Code " + response.statusCode());
		assertEquals(expectedStatusCode, response.statusCode());
		responseDataValidation(updatedPayload, resourceType, actualResponse, expectedErrorMessage);
	}

	
	// ***Converting Custom Id as Blank field 
	public String convertCustomIdAsBlank(String validationCustomFieldType) throws JsonProcessingException {
		DataSet ds = new DataSet();
		List<String> data = ds.getDataList(CommonLib.sheetPath, CommonLib.sheetName, CommonLib.rowIndex);
    	resourceType = data.get(0);
    	DataDriven d = new DataDriven(CommonLib.sheetPath);
		d.getColumnCount(CommonLib.sheetName);
		Gson gson = new Gson();
		gson.fromJson(CommonLib.payloads, EmailAcquisition.class);
		EmailAcquisition strToJson = gson.fromJson(CommonLib.payloads, EmailAcquisition.class);
		ArrayList<Custom> mylist = new ArrayList<Custom>();
		//Setting Custom Id as blank
		
		if(validationCustomFieldType.equals("customFieldId")) {
		CommonLib.customFieldId = data.get(50); // test26
		Value = data.get(51); // value26
		mylist.add(new Custom(" ", data.get(51)));	
		}else if(validationCustomFieldType.equals("customFieldValue")) {
			
			Id = data.get(50); // value26
			CommonLib.customFieldValue=data.get(51);
			mylist.add(new Custom( Id, " "));	
		}else if(validationCustomFieldType.equals("invalidCustomFieldValue")) {
			Id = data.get(50); // value26
			
			mylist.add(new Custom( Id, "InvalidValue"));
		}
		int colCount=CommonLib.colCount;
		for (int t = 52; t <= colCount - 2; t++) {
			Id = data.get(t); // test26
			Value = data.get(t + 1); // value26
			if (Id != null & Value!=null) {
				mylist.add(new Custom(Id, Value));
//				t++;
			}else if(t==colCount) {
				break;
				}else if(data.get(t)==null && data.get(t+1)==null) {
						break;					
				}			
			t++;
		}
		
		
		strToJson.setCustom(mylist);
		ObjectMapper obj = new ObjectMapper();
		obj.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
		String payloads = obj.writerWithDefaultPrettyPrinter().writeValueAsString(strToJson);
		FileLoggers.info("*********payload with Blank Custom Id field**************");
		FileLoggers.info(payloads);
		return payloads;
	}
	
	
	public void responseDataValidation(String payload, String resourceType,String actualResponse, String expectedErrorMessage)  {
		JSONObject jsobj = new JSONObject(payload);
		templateId = jsobj.getString("id");
		JsonPath js = new JsonPath(actualResponse);
		Boolean b = js.get("results.validationResult.success");
		// Validating Error Message
		if (b != true) {
			String errorMessage = js.get("results.validationResult.errorMessages[0]");
			FileLoggers.info("Error message received is --> " + errorMessage);
			FileLoggers.info("Error message expected is --> " + expectedErrorMessage);
			assertEquals(errorMessage,expectedErrorMessage);
		} else {
			FileLoggers.info("Request response received is Success");
		}
		// Validating Request Id as null
		requestId = js.get("requestId");
		assertEquals(null, requestId);
		// Validating Response Id as null
		responseId = js.get("responseId");
		Assert.assertNotNull(responseId);
		// Validating Response Page
		FileLoggers.info("Response Page received --> " + js.get("responsePage"));
		ResponsysPage resopnsePage = new ResponsysPage();
        String ResponsePage = resopnsePage.createUrl(resourceType,templateId, actualResponse);
        FileLoggers.info("Response Page Expected --> " + ResponsePage);
        assertEquals(ResponsePage, js.get("responsePage")); 
	}
	
	public void flagValidation(String resourceType, String responseReceived, String flagToCheck,String flagContainer, String expectedFlag){
		String templateType = resourceType;
		String completeFlag = null;
		//Create the flag path to be checked 
		 if (flagContainer.isEmpty()) {
			 completeFlag =flagToCheck;
		 }else if(flagToCheck.isEmpty()){
			 completeFlag="results."+flagContainer;
		 }else {
			 completeFlag="results."+flagContainer+"."+flagToCheck;
		 }
		 
		 if (expectedFlag.equals("null")) {
			 expectedFlag=null;
		 }
		 
		//validating the flag
		JsonPath jsonPathResponse = new JsonPath(responseReceived);
		FileLoggers.info(" **********   verified "+completeFlag+": and status is "+jsonPathResponse.getString(completeFlag)+" ********");
		assertEquals(jsonPathResponse.getString(completeFlag), expectedFlag);
		
	}
	public void responsePageValidation(String payload, String resourceType,String actualResponse) {
		ResponsysPage resopnsePage = new ResponsysPage();
		JSONObject jsobj = new JSONObject(payload);
		templateId = jsobj.getString("id");
		JsonPath jpResponse = new JsonPath(actualResponse);
        String ResponsePage = null;
			ResponsePage = resopnsePage.createUrl(resourceType,templateId, actualResponse);
        FileLoggers.info("Response Page Expected --> " + ResponsePage);
        assertEquals(ResponsePage, jpResponse.get("responsePage")); 
	
	}
	
	public static void fetchCustomFieldLabelData() throws IOException, SQLException {
		FileLoggers.info("******************* Fetching Field Data for Custom Labels**********");
		FileLoggers.info("*******************                              **********");
		HashMap<String, String> customFieldData = new HashMap<String, String>();
		String query = "SELECT FieldId, label FROM  `field`" + ";";	
		ResultSet rsa = CommonLib.db.getSQLResults(query);
		while (rsa.next()) {
			customFieldData.put(rsa.getString(1), rsa.getString(2));
		}
		CommonLib.customFieldData=customFieldData;
	}
	
	/*
	 * public ResultSet getSqlResults(String query) { DataBaseConnection db = new
	 * DataBaseConnection(); ResultSet rsa = null; try { rsa =
	 * db.getSQLResults(query); } catch (IOException e) { // TODO Auto-generated
	 * catch block e.printStackTrace(); } catch (SQLException e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); } return rsa;
	 * 
	 * }
	 */
}
