package common;

// PropertyConfigurator;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileLoggers {
	
	 //private static final org.apache.logging.log4j.Logger Log = LogManager.getLogger(FileLoggers.class);
	static Logger Log;
	
	public static void initLogger() {
		System.setProperty("log4j.configurationFile", "./log4j2.xml");
		 Log = LoggerFactory.getLogger(FileLoggers.class);
		//Logger logger = LoggerFactory.getLogger(Demo.class);
		//PropertyConfigurator.configure("log4j.properties");
		//api
		System.out.println(org.apache.logging.log4j.Logger.class.getResource("/Logger.class"));
		//core
		System.out.println(org.apache.logging.log4j.Logger.class.getResource("/Appender.class"));
		//config
		System.out.println(org.apache.logging.log4j.Logger.class.getResource("/log4j2.xml"));
		
	}

	public static void startTestCase(String TestCaseName) {
//		Log.info("********************************************************************");
		Log.info("********************************************************************");
		Log.info("********************************************************************");
		Log.info("******************          " + TestCaseName + "     *******************");
		Log.info("********************************************************************");
//		Log.info("********************************************************************");

	}

	public static void endTestCase() {
//		Log.info("********************************************************************");
//		Log.info("********************************************************************");
		Log.info("********************************************************************");
		Log.info("***********       " + "   E--N--D TestCase " + "     *******************");
		Log.info("********************************************************************");
//		Log.info("********************************************************************");
//		Log.info(" ");
//		Log.info(" ");
		Log.info(" ");
	}

	public static void info(String message) {
		Log.info(message);
	}

	public static void warn(String message) {
		Log.info(message);
	}

	public static void error(String message) {
		Log.info(message);
	}

	public static void fatal(String message) {
		Log.info(message);
	}

	public static void debug(String message) {
		Log.info(message);
	}

}
