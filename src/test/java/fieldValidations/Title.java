package fieldValidations;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.json.JSONObject;
import org.junit.Assert;

import common.FileLoggers;
import common.ReplaceFieldValuePayload;
import common.RequestAPI;
import common.ResponseAPI;
import common.Utils;
import common.ValidationMessages;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Title extends Utils{


	RequestSpecification req;
	ReplaceFieldValuePayload pup = new ReplaceFieldValuePayload();
	String payload_updated;
	Response response;
	RequestAPI api = new RequestAPI();
	ResponseAPI respapi = new ResponseAPI();
	int expectedStatusCode = 400;
	String requestId;
	String responseId;
	String templateId;
	String expectedErrorMessage = "Email Field format is invalid";
//	String expectedErrorMessage = " Field value is invalid";
//	String ExpectedMaxLengthErrorMessage = "Email Field should be max length 128";
//	String Expected_MaxLengthErrorMessage = " Field should be max length 50";
	String responsePage;

	
	public void validatingAllTitleScenarios(String methodName, String resourceType, String payload) throws IOException {
		titleExceedingMaxLength(methodName,  resourceType,  payload);
		titleAcceptingAlphanumeric(methodName,  resourceType,  payload);
		titleAcceptingSpecialCharacters(methodName,  resourceType,  payload);
		
	}
	

	public void titleExceedingMaxLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Title Fields - titleExceedingMaxLength scenario*****");
		String fieldName = "title";
		String fieldValue = "titlewithexceedingmaxlengthinrequestformatforfieldvalidation";
		ValidationMessages message = ValidationMessages.valueOf("TITLEEXCEEDINGMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		titleAcceptingAlphanumeric(methodName,  resource,  payload);
	}

	public void titleAcceptingAlphanumeric(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Title Fields - titleAcceptingAlphanumeric scenario*****");
		String fieldName = "title";
		String fieldValue = "title2345";
		expectedErrorMessage = "";
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 200, expectedErrorMessage);		
//		titleAcceptingSpecialCharacters(methodName,  resource,  payload);
	}
	
	public void titleAcceptingSpecialCharacters(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Title Fields - titleAcceptingSpecialCharacters scenario*****");
		String fieldName = "title";
		String fieldValue = "@#$%^&*(){";
		expectedErrorMessage = "";
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 200, expectedErrorMessage);
		FileLoggers.info("Validation Title Fields Completed");
	}
	
	








}
