package fieldValidations;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.json.JSONObject;
import org.junit.Assert;

import common.FileLoggers;
import common.ReplaceFieldValuePayload;
import common.RequestAPI;
import common.ResponseAPI;
import common.Utils;
import common.ValidationMessages;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class FirstName extends Utils{

	RequestSpecification req;
	ReplaceFieldValuePayload pup = new ReplaceFieldValuePayload();
	Response response;
	RequestAPI api = new RequestAPI();
	ResponseAPI respapi = new ResponseAPI();
	int expectedStatusCode = 400;
	String requestId;
	String responseId;
	String templateId;
	String expectedErrorMessage = "Email Field format is invalid";
//	String expectedErrorMessage = " Field value is invalid";
//	String expectedMaxLengthErrorMessage = "Email Field should be max length 128";
//	String expected_MaxLengthErrorMessage = " Field should be max length 50";
	String ResponsePage;
	
	public void validatingAllFirstNameScenarios(String methodName, String resourceType, String payload) throws IOException {
		firstNameExceedingMaxLength(methodName,  resourceType,  payload);
		firstNameAcceptingAlphanumeric(methodName,  resourceType,  payload);
		firstNameAcceptingSpecialCharacters(methodName,  resourceType,  payload);
	}

	
	public void firstNameExceedingMaxLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating FirstName Fields firstNameExceedingMaxLength scenario*****");
		String fieldName = "firstName";
		String fieldValue = "firstNamewithexceedingmaxlengthinrequestformatforfieldvalidation";
		ValidationMessages message = ValidationMessages.valueOf("FIRSTNAMEEXCEEDINGMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		firstNameAcceptingAlphanumeric(methodName,  resource,  payload);
	}

	public void firstNameAcceptingAlphanumeric(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating FirstName Fields - firstNameAcceptingAlphanumeric scenario*****");
		String fieldName = "firstName";
		String fieldValue = "firstName12345";
		expectedErrorMessage = "";
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 200, expectedErrorMessage);		
//		firstNameAcceptingSpecialCharacters(methodName,  resource,  payload);
	}
	
	public void firstNameAcceptingSpecialCharacters(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating FirstName Fields - firstNameAcceptingSpecialCharacters scenario*****");
		String fieldName = "firstName";
		String fieldValue = "firstName!@#$%^&*(){}~!12345";
		expectedErrorMessage = "";
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 200, expectedErrorMessage);
		FileLoggers.info("*****Validation FirstName Fields Completed*****");
	}
	
	



}
