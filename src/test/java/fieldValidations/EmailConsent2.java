package fieldValidations;

import java.io.IOException;

import common.FileLoggers;
import common.Utils;
import common.ValidationMessages;

public class EmailConsent2 extends Utils{
	String expectedErrorMessage;
	
	public void EmailConsent2Mandatory(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("Validating EmailConsent2 Fields");
		String fieldName = "emailconsent2";
		String fieldValue = "";		
//		expectedErrorMessage = "At least one Email consent must be checked";
		ValidationMessages message = ValidationMessages.valueOf("EMAILCONSENT2MANDATORY");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		(MethodName,  resource,  Payload);
		FileLoggers.info("Validation EmailConsent2 Fields Completed");
	}
}
