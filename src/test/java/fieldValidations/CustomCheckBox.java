package fieldValidations;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.json.JSONObject;

import common.CommonLib;
import common.DataBaseConnection;
import common.FileLoggers;
import common.ReplaceFieldValuePayload;
import common.Utils;

public class CustomCheckBox extends Utils {
	String expectedErrorMessage;
	String templateId;
	String updatedPayload;

	public void customcheckboxSingleInValidValue(String methodName, String resourceType, String payload,
			String CustomFieldID, String CustomfieldValue, int setCount) throws IOException {	
		FileLoggers.info("*******Inside Custom validation method**********");
		FileLoggers.info("Validating customcheckboxSingleValidValue Fields");
		String fieldName = CustomFieldID;// CustomFieldID;
		String fieldValue = "Invalid";
//		expectedErrorMessage = CustomFieldID + " Field value is invalid";
		expectedErrorMessage = CommonLib.customFieldLabel + " Field value is invalid";		
		String payload_updated;
		ReplaceFieldValuePayload payloadReplace = new ReplaceFieldValuePayload();
		payload_updated = payloadReplace.customReplaceFieldValue(payload, fieldName, fieldValue, setCount);
		// ****************** Validation check for Custom Fields
		customResponseValidation(payload_updated, fieldName, fieldValue, resourceType, methodName, 400,
				expectedErrorMessage);
		}
	

	public void customMissingID(String methodName, String resourceType, String payload, String CustomFieldID,
			String CustomfieldValue, int setCount, String expectedErrorMessage, int Flag) throws IOException {
		FileLoggers.info("*******Inside Custom Missing ID validation method**********");
		String fieldName = "";// CustomFieldID;
		String fieldValue = CustomfieldValue;		
		ReplaceFieldValuePayload payloadReplace = new ReplaceFieldValuePayload();
		updatedPayload = payloadReplace.customRemoveFieldId(payload, fieldName, fieldValue, setCount);
		// ****************** Validation check for Custom Fields
		customResponseValidation(updatedPayload, fieldName, fieldValue, resourceType, methodName, 400,
				expectedErrorMessage);
		if(Flag!=0) {
		customcheckboxSingleInValidValue(methodName,  resourceType,  payload,  CustomFieldID,
				 CustomfieldValue,  setCount);
		}
	}
	
	public void customMultipleValueCheck(String methodName, String resourceType, String payload, String CustomFieldID,
			String CustomfieldValue, int setCount) throws IOException,SQLException {
		JSONObject jsobj = new JSONObject(payload);
		templateId = jsobj.getString("id");
		FileLoggers.info("*******Inside Custom MultipleValueCheck validation method**********");
		FileLoggers.info("Validating customMultipleValueCheck Fields");
		// Getting Custom valid values from DB
		String query = "SELECT fo.VALUE FROM field f INNER JOIN form_field ff ON f.FieldId = ff.FieldId INNER JOIN Template t ON t.TemplateKey = ff.TemplateKey INNER JOIN `field_option` fo ON fo.FieldId=ff.FieldId WHERE f.FieldId="
				+ "'" + CustomFieldID + "'" + "AND t.templateId =" + "'" + templateId + "'";
		System.out.println(query);
//		DataBaseConnection db = new DataBaseConnection();
		ResultSet rsa = CommonLib.db.getSQLResults(query);		
		ArrayList<String> mylist1 = new ArrayList<String>();
		while (rsa.next()) {
			String data = rsa.getString("VALUE");
			mylist1.add(data);
		}
		System.out.println(mylist1);		
		String fieldName = CustomFieldID;// CustomFieldID;
		String fieldValue = mylist1.toString().replaceAll(" ","");
		fieldValue = fieldValue.replaceAll("[\\[\\](){}]","");
		System.out.println(fieldValue);
		expectedErrorMessage = ""; //CustomFieldID + " Field is required";		
		ReplaceFieldValuePayload payloadReplace = new ReplaceFieldValuePayload();
		updatedPayload = payloadReplace.customReplaceFieldValue(payload, fieldName, fieldValue, setCount);
		// ****************** Validation check for Custom Fields
		customResponseValidation(updatedPayload, fieldName, fieldValue, resourceType, methodName, 200,
				expectedErrorMessage);
//		}
	}
}
